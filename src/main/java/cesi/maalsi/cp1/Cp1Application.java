package cesi.maalsi.cp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class Cp1Application {

	public static void main(String[] args) {
		SpringApplication.run(Cp1Application.class, args);
	}

}
